# Zip of Interest 

```sql
create table `quarre.DataResearchFiles.zip_interest` as

select 
  county_names.county_name as county,
  zips.USPS_ZIP_PREF_STATE as state,
  zips.ZIP as zip,
  zips.USPS_ZIP_PREF_CITY as city,
  false as is_active,
  '' as description

from 
  `quarre.DataResearchFiles.raw_geo_county_fips_master` as county_names,
  `quarre.DataResearchFiles.raw_geo_county_fips_to_zip` as zips

where 
  zips.COUNTY = county_names.fips

order by 
  state, county, zip
```
