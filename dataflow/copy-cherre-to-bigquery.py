import simplejson as json
import tempfile
from cherreUtil import get_ny_court_jdg_data_for_date_range, get_nyscef_court_data_for_date_range, get_ny_court_cas_data_for_date_range, get_ny_court_cases_data_for_date_range
from googleStorageUtil import upload_blob_from_stream, load_gcs_to_big_query
from datetime import datetime, date

# 
# Script that copies data from Cherre's ny_court datasets to BigQuery.
# Along the way we store the raw data in a Google Storage bucket.
#


### Data mapping

object_fn_map = {
  "ny_court_jdg": get_ny_court_jdg_data_for_date_range,
  "nyscef_court": get_nyscef_court_data_for_date_range,
  "ny_court_cas": get_ny_court_cas_data_for_date_range,
  "ny_court_cases": get_ny_court_cases_data_for_date_range
}


### Configuration.

# Name of the object from cherre.
object_name = "ny_court_cas"

# Begin date of the query at cherre.
begin_date = "2022-09-01"

# Name of the date for the bucket.
upload_date = datetime.today().strftime('%Y%m%d')
blob_path = f"{object_name}/{upload_date}/data.ndjson"
gcs_blob_uri = f"gs://quarre-staging/{blob_path}"

# Path to the schema for the Bigquery table.
schema_json_filepath = "./schema/big-query/ny-court-cas.json"

# Bigquery table name parts and whole.
project_id = "quarre"
dataset_id = "DataResearchFiles"
table_id = object_name
dataset_and_table_id = f"{dataset_id}.{table_id}"


### Code starts here.

# Create a temporary file for the cherre download.
tempfile_ndjson = tempfile.NamedTemporaryFile(suffix=".ndjson")

# Download form cherre into the temp file.
rowCount = object_fn_map[object_name](begin_date, "unused-enddate-TODO", tempfile_ndjson)
print(f"Got rows count {rowCount}")

# Upload the temp file to google storage.
upload_blob_from_stream("quarre-staging", tempfile_ndjson, blob_path)

# Copy from the bucket to bigquery.
with open(schema_json_filepath) as json_file:
  target_schema = json.load(json_file)
load_gcs_to_big_query(gcs_blob_uri, dataset_and_table_id, target_schema)
