#
# Apache beam pipeline that creates contact information for
# owners that don't already have it.
#
# Code inspired by https://medium.com/datamindedbe/how-to-build-a-cleaning-pipeline-with-bigquery-and-dataflow-on-gcp-3d2f288d4e1b
#
#

import apache_beam as beam
import simplejson as json
import requests
import re


from apache_beam.io import fileio
from apache_beam.io import ReadFromBigQuery
from apache_beam.io import WriteToBigQuery
from apache_beam.io.gcp.bigquery_tools import parse_table_schema_from_json
from apache_beam.io.gcp.internal.clients import bigquery
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions

from google.cloud import bigquery
from google.cloud.exceptions import NotFound
from google.oauth2 import service_account


from AddOwnerContactInfo import NormalizeOwnerContactInfo, QueryContactInfoSource
from quarreUtil import *


options = PipelineOptions()
google_cloud_options = options.view_as(GoogleCloudOptions)
google_cloud_options.project = "quarre"
google_cloud_options.job_name = "add-test-contact-info"
google_cloud_options.staging_location = "gs://quarre-staging/dataflow/staging"
google_cloud_options.temp_location = "gs://quarre-staging/dataflow/temp"
options.view_as(StandardOptions).runner = "DirectRunner"  # use this for debugging
#options.view_as(StandardOptions).runner = "DataFlowRunner"


#credentials = service_account.Credentials.from_service_account_file("./quarre-6322d49de7bc.json")


def get_query_for_contact_provider(contact_source_id):
    # TODO - use a real SQL formatting tool.
    query = f"""

-- Input from the owner_interest table with some filtering unique to various experiments.

with owner_interest_union as (

select * from `quarre.DataResearchFiles.Owner_Interest_Final` limit 1

)
, 

-- Filter out owners that already have contact info

owner_interest_by_owner_id as
(
    select
        owner_interest.*,
        row_number() over(partition by owner_interest.quarre_owner_id order by owner_interest.tax_assessor_id) as owner_id_count,
        row_number() over(partition by owner_interest.query_type order by owner_interest.query_type, owner_interest.owner_id, owner_interest.tax_assessor_id) as query_type_count
    from owner_interest_union owner_interest
    where not exists (
        select * from `quarre.DataResearchFiles.owner_contact` owner_contact
        where owner_contact.contact_source_id = '{contact_source_id}'
        and owner_contact.quarre_owner_id = owner_interest.quarre_owner_id
    ) and owner_interest.owner_id is not null

)

-- Getting the data we want to find contact info for.

select 
    quarre_owner_id,
    tax_assessor_id, 
    owner_id, 
    owner_name, 
    first_name, middle_name, last_name, 
    party_street as party_address, party_city, party_state, party_zip, 
    mailing_street as mailing_address, mailing_city, mailing_state, mailing_zip, 
    property_street as property_address, property_city, property_state, property_zip

from owner_interest_by_owner_id
where owner_interest_by_owner_id.owner_id_count = 1
and owner_interest_by_owner_id.quarre_owner_id is not null
order by owner_interest_by_owner_id.query_type_count,
owner_interest_by_owner_id.query_type,
owner_interest_by_owner_id.owner_id
limit 500


    """
    return query

        
##### Code starts here #####

project_id = "quarre"
client = bigquery.Client(project=project_id)#credentials=credentials)

# api_response_table_id = "DataResearchFiles.api_response"
# api_response_schema_json_filepath = "./schema/big-query/api-response.json"

# with open(api_response_schema_json_filepath) as json_file:
#     target_schema = json.load(json_file)
#     create_bq_table_if_not_exists(client, api_response_table_id, target_schema["fields"])

owner_contact_dataset_id = "DataResearchFiles"
owner_contact_table_id = "owner_contact"
owner_contact_schema_json_filepath = "./schema/big-query/owner-contact-info.json"

owner_contact_fully_qualified_table_id = f"{project_id}:{owner_contact_dataset_id}.{owner_contact_table_id}"

# Lazily create the table if it doesn't exist.
with open(owner_contact_schema_json_filepath) as json_file:
    target_schema = json.load(json_file)
    create_bq_table_if_not_exists(client, owner_contact_dataset_id, owner_contact_table_id, target_schema["fields"])


contact_source_id = "IDI"

query = get_query_for_contact_provider(contact_source_id)

# Start of beam pipeline configuration.

source = ReadFromBigQuery(query=query, use_standard_sql=True)
target = WriteToBigQuery(table=owner_contact_fully_qualified_table_id, schema=target_schema, write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND)

class JsonSink(fileio.TextSink):
    def write(self, record):
        self._fh.write(json.dumps(record, indent=2).encode('utf-8'))



def run():
    with beam.Pipeline(options=options) as p:
        api_response = (
            p 
            | "ReadTable" >> beam.io.Read(source) 
            | "QueryContactInfo" >> beam.ParDo(QueryContactInfoSource())
            )
        side_effect = (
            api_response 
            | "WriteApiResponse" >>  beam.io.fileio.WriteToFiles(
                path=f"gs://quarre-staging/api_responses/{contact_source_id}/",
                destination=lambda record: re.sub(r'[^a-zA-Z0-9]', '_', record["quarre_owner_id"]),
                file_naming=beam.io.fileio.destination_prefix_naming(suffix=".json"),
                sink=JsonSink())
            )
        big_query_writer = (
            api_response
            | "NormalizeContactInfo" >> beam.ParDo(NormalizeOwnerContactInfo())
            | "WriteToBigQuery" >> beam.io.Write(target)
            )

if __name__ == "__main__":
    run()
