import apache_beam as beam

#
# Prints to stdout.
#
#
class LogElements(beam.DoFn):
  """
  Side effect to show what the pipeline is up to.
  """

  def __init__(self):
    print("Initializing LogElements")

  def process(self, row):
    print("Got a row", row)
    return [row]
