import simplejson as json
import tempfile
from cherreUtil import get_ny_court_jdg_data_for_date_range, get_nyscef_court_data_for_date_range, get_ny_court_cas_data_for_date_range, get_ny_court_cases_data_for_date_range
from googleStorageUtil import upload_blob_from_stream, load_gcs_to_big_query
from datetime import datetime, date
import requests
import argparse

### Pacer Configuration.

prodAuthUrl = "https://pacer.login.uscourts.gov"
qaAuthUrl = "https://qa-login.uscourts.gov"

qaApiUrl = "https://qa-pcl.uscourts.gov"
prodApiUrl = "https://pcl.uscourts.gov"

prodUsername = "quarrepacer"
prodPassword = "REQuants18!"

qaUsername = "pacerquarre"
qaPassword = "REQuants18!"

newTokenHeaderKey = "X-NEXT-GEN-CSO"

authEndpoint = f"{prodAuthUrl}/services/cso-auth"
apiCasesEndpoint = f"{prodApiUrl}/pcl-public-api/rest/cases/find"


### Pacer Functions.

# Get the auth token.
def doAuthQuery(url):

  headers = { 
    "Content-type": "application/json",
    "Accept": "application/json"
  }
  
  payload = {
    "loginId": prodUsername,
    "password": prodPassword
  }
  jsonPayload = json.dumps(payload, indent=4, sort_keys=False, default=str)

  print("aobut to auth", url)
  print("headers", json.dumps(headers, indent=4, sort_keys=False, default=str))
  print("data", jsonPayload)
  timeoutSeconds = 30
  r = requests.post(url, timeout = timeoutSeconds, headers = headers, data = jsonPayload)
  if r.status_code == requests.codes.ok:
    print("got ok", r.json())
    jsonResult = r.json()
    if jsonResult["loginResult"] == "0":
      return jsonResult["nextGenCSO"]
    return None
  else:
    print("Error is")
    print(r)
    raise("Couldn't do authentication.")

# Get one page of results from a pacer query.
def doApiQuery(url, authToken, query, sortBy, pageNum = 0):
  headers = { 
    "Content-type": "application/json",
    "Accept": "application/json",
    "X-NEXT-GEN-CSO": authToken
  }
  jsonPayload = json.dumps(query, indent=4, sort_keys=False, default=str)
  params = {
    "sort": sortBy,
    "page": pageNum
  }

  timeoutSeconds = 30

  print("url", url)
  print("headers", json.dumps(headers, indent=4, sort_keys=False, default=str))
  print("body", jsonPayload)

  r = requests.post(url, timeout = timeoutSeconds, headers = headers, data = jsonPayload, params = params)
  if r.status_code == requests.codes.ok:
    print("got ok", json.dumps(r.json(), indent=4, sort_keys=False, default=str))
    jsonResult = r.json()
    return jsonResult
  else:
    print("Error code", r.status_code)
    print(r.content)
    raise("Couldn't do query.")

def jsonRowsToString(rows):
  rowsAsString = list(map(lambda x: json.dumps(x) + "\n", rows))
  return rowsAsString

def writePacerToFile(query, sortBy, authToken, outputFile):
  pageNum = 0
  hasMorePages = True
  while hasMorePages:

    print("queried page", pageNum)

    jsonResult = doApiQuery(apiCasesEndpoint, authToken, query, sortBy, pageNum)

    hasMorePages = jsonResult["pageInfo"]["totalPages"] > pageNum + 1
    caseRows = jsonResult["content"]

    outputFile.writelines(jsonRowsToString(caseRows))

    pageNum += 1
  return pageNum


### Configuration.

court_id = "nysbk"
query = {
  "courtId": [court_id],
  "dateFiledFrom": "2022-10-04",
  "dateFiledTo": "2022-12-31"
}
sortBy = ["dateFiled,ASC", "caseId,ASC"]


# Name of the date for the bucket.
upload_date = datetime.today().strftime('%Y%m%d')
blob_path = f"pacer/{court_id}/{upload_date}/data.ndjson"
gcs_blob_uri = f"gs://quarre-staging/{blob_path}"

# Path to the schema for the Bigquery table.
schema_json_filepath = "./schema/big-query/pacer.json"

# Bigquery table name parts and whole.
project_id = "quarre"
dataset_id = "DataResearchFiles"
table_id = "pacer"
dataset_and_table_id = f"{dataset_id}.{table_id}"


### Code starts here.

authToken = doAuthQuery(authEndpoint)

# Create a temporary file for the cherre download.
tempfile_ndjson = tempfile.NamedTemporaryFile(suffix=".ndjson")

# Download form cherre into the temp file.
pageCount = writePacerToFile(query, sortBy, authToken, tempfile_ndjson)
print(f"Got rows count {pageCount}")

# Upload the temp file to google storage.
upload_blob_from_stream("quarre-staging", tempfile_ndjson, blob_path)

# Copy from the bucket to bigquery.
with open(schema_json_filepath) as json_file:
  target_schema = json.load(json_file)
load_gcs_to_big_query(gcs_blob_uri, dataset_and_table_id, target_schema)
