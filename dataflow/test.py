from cherreUtil import get_ny_court_cas_data_for_date_range


with open('ny_court_cas_outer_counties.ndjson', 'wb') as f:
  rowCount = get_ny_court_cas_data_for_date_range("2020-01-01", "", f)
  print(f"Got rows count {rowCount}")
