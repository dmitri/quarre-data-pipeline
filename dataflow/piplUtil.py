from benedict import benedict


def get_Pipl_owner_contact_rows(owner_row):
    url = "https://api.pipl.com/search/"
    params = {
        "raw_name": owner_row["owner_name"],
        "raw_address": owner_row["mailing_one_line_address"],
        "match_requirements": "phone",
        "top_match": "true",
        "key": "bk3y8mkx0slpa09mm6crcztb"
    }
    res = do_get_with_retry(url, params=params)
    if res is not None:
        return res.json()
    else:
        return None


def transform_raw_Pipl_json_to_normalized_owner_contact_row(pipl_api_response):
        
    print("will transform", pipl_api_response)


    d = benedict(pipl_api_response["api_response"])

    new_row = {
        "phone_numbers": []
    }
        
    # See if any phone numbers were returned and add them 
    key_path = "person.phones"
    if key_path in d:
        phone_number_entries = d[key_path]
        phone_numbers = list(map(lambda phone_number_entry: phone_number_entry["display"], phone_number_entries))
        new_phone_number_rows = []
        for phone_number in phone_numbers:
            new_phone_number_row = {
                "is_confirmed": False,
                "phone_number": phone_number
            }
            new_phone_number_rows.append(new_phone_number_row)
        new_row["phone_numbers"] = new_phone_number_rows
        
    return new_row