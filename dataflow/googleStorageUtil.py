from google.cloud import storage, bigquery

# https://cloud.google.com/storage/docs/samples/storage-stream-file-upload#storage_stream_file_upload-python
def upload_blob_from_stream(bucket_name, file_obj, destination_blob_name):
  """Uploads bytes from a stream or other file-like object to a blob."""
  # The ID of your GCS bucket
  # bucket_name = "your-bucket-name"

  # The stream or file (file-like object) from which to read
  # import io
  # file_obj = io.BytesIO()
  # file_obj.write(b"This is test data.")

  # The desired name of the uploaded GCS object (blob)
  # destination_blob_name = "storage-object-name"

  # Construct a client-side representation of the blob.
  storage_client = storage.Client()
  bucket = storage_client.bucket(bucket_name)
  blob = bucket.blob(destination_blob_name)

  # Rewind the stream to the beginning. This step can be omitted if the input
  # stream will always be at a correct position.
  file_obj.seek(0)

  # Upload data from the stream to your bucket.
  blob.upload_from_file(file_obj)

  print(
      f"Stream data uploaded to {destination_blob_name} in bucket {bucket_name}."
  )
  
# https://cloud.google.com/bigquery/docs/loading-data-cloud-storage-csv
def load_gcs_to_big_query(gcs_path, target_table, target_schema):
  print(f"About to load {gcs_path} to bigquery.")
  project_id = "quarre"
  bq_client = bigquery.Client(project=project_id)
  job_config = bigquery.LoadJobConfig(
    source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON, 
    schema=target_schema, 
    create_disposition="CREATE_IF_NEEDED",
    write_disposition="WRITE_APPEND"
  )
  job = bq_client.load_table_from_uri(gcs_path, target_table, job_config=job_config)
  job.result()  # Waits for the job to complete.
  print(f"Loaded {gcs_path} to bigquery.")