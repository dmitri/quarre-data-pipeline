import requests
from google.cloud import bigquery
from google.api_core.exceptions import NotFound
import re

##### Library functions #####
#
# TODO - move these to common code files as needed elsewhere.
#

quarre_dataset = "DataResearchFiles"

def does_bq_table_exist(client, dataset_id, table_id):
    try:
        client.get_table(f"{dataset_id}.{table_id}")
        return True
    except NotFound:
        return False

def create_bq_table_if_not_exists(client, dataset_id, table_id, schema):
    if not does_bq_table_exist(client, dataset_id, table_id):
        tableRef = client.dataset(dataset_id).table(table_id)
        table = bigquery.Table(tableRef, schema=schema)
        client.create_table(table)

def do_get_with_retry(url, params = {}, retry_count = 10):
    timeout_seconds = 30
    r = requests.get(url, timeout=timeout_seconds, params=params)
    try:
        if r.status_code == requests.codes.ok:
            return r
        else:
            return None
    except:
        return None
        



#### Data parsing functions #####

def parse_address_line(address):
    addressRegex = re.compile("^(?P<street>.*), (?P<city>.*), (?P<state>.*) (?P<zip>\d{5})$")

    addressMatch = addressRegex.match(address)

    if addressMatch is None:
        return None
    
    return {
        "address": addressMatch.group("street"),
        "city": addressMatch.group("city"),
        "state": addressMatch.group("state"),
        "zip": addressMatch.group("zip")
    }

def parse_person_name(name):
    tokens = name.split()

    if len(tokens) == 1:
        return {
            "lastName": tokens[0]
        }
    if len(tokens) == 2:
        return {
            "firstName": tokens[0],
            "lastName": tokens[1]
        }
    if len(tokens) == 3:
        return {
            "firstName": tokens[0],
            "middleName": tokens[1],
            "lastName": tokens[2]
        }

    return {
        "firstName": tokens[0],
        "lastName": tokens[-1]
    }
