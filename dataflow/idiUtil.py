from http import client
import simplejson as json
import base64
import re
from webbrowser import get
import requests
import time

def get_IDI_authentication_token():

    #authenticationUrl = "https://login-api-test.idicore.com/apiclient"
    #clientid = "api-client@quarre_test" 
    #clientsecret = "nEB3KMDFdSse64aaCE!%p5kM9cH6LP6t%J64i8dWkQikG2Zk$dC7NSRdgwXWnMy7"
    #clientsecret = "NQQcdSe7X2vzQrCzpath3XTCDNEiyqmWQUmKyrqAj%PH6MzWJBr!QKrrxRreHRGv"

    authenticationUrl = "https://login-api.idicore.com/apiclient"
    clientid = "api-client@quarre"
    clientsecret = "CKVJ8%HFwndjgSHLAp3x3ZdhW%JaPir9gs&fXEGci25%aABPnqh%AjB5zyVNfqkt"

    # Get the authentication token.
    authString = base64.b64encode((f"{clientid}:{clientsecret}").encode('ascii'))

    payload = {
        "glba": "otheruse",
        "dppa": "none"
    }

    jsonPayload = json.dumps(payload, indent=4, sort_keys=False, default=str)

    authHeaderValue = f"Basic {authString.decode('ascii')}"

    headers = {
        "Authorization": authHeaderValue,
        "Content-type": "application/json"
    }

    #print(json.dumps(headers, indent=4, sort_keys=False, default=str))

    response = requests.request("POST", authenticationUrl, data=jsonPayload, headers=headers)
    if (response.status_code != 200):
        print(f"Authentication failed with error {response.status_code}")
        return None

    authToken = response.text

    return authToken


def get_IDI_owner_contact_for_address(firstName, middleName, lastName, businessName, address, city, state, zip, authToken):

    #searchUrl = "https://api-test.idicore.com/search"
    searchUrl = "https://api.idicore.com/search"

    queryFields =  ["name", "phone", "address", "email"]
    if businessName is not None:
        queryFields = ["business"]

    payload = {
        "fields": queryFields,
        "address": address,
        "city": city,
        "state": state,
        "zip": zip,
        "lastName": lastName,
        "firstName": firstName,
        "middleName": middleName,
        "businessName": businessName
    }

    for k, v in dict(payload).items():
        if v is None or v == "":
            del payload[k]

    if "firstName" in payload:
        payload["nicknamesearch"] = True

    headers = {
        "authorization": authToken,
        "content-type": "application/json",
        "accept": "application/json"
    }

    jsonPayload = json.dumps(payload, indent=4, sort_keys=False, default=str)

    #print("will query IDI for owner contact")
    #print(json.dumps(payload, indent=4, sort_keys=False, default=str))

        
    start = time.perf_counter_ns()


    response = requests.request("POST", searchUrl, data=jsonPayload, headers=headers)


    duration = time.perf_counter_ns() - start
    print(f"Response from IDI in {duration // 1000000}ms.")


    if (response.status_code != 200):
        print(f"Search failed with error {response.status_code}")
        print("Request payload:")
        print(json.dumps(payload, indent=4, sort_keys=False, default=str))
        print("Response:")
        print(response.text)
        return None

    # print(json.dumps(response.json(), indent=4, sort_keys=False, default=str))
    # print(response.status_code)

    return response.json()


def get_IDI_owner_contact_for_address_type(address_type, owner_row, authToken, match_technique, address_specificity):

    first_name = None
    middle_name = None
    last_name = None
    business_name = None
    
    if match_technique == "Reverse_name":
        last_name = owner_row["first_name"]
        middle_name = owner_row["middle_name"]
        first_name = owner_row["last_name"]
    
    if match_technique == "Normal_name":
        first_name = owner_row["first_name"]
        middle_name = owner_row["middle_name"]
        last_name = owner_row["last_name"]

    if match_technique == "Business_name":
        business_name = owner_row["owner_name"]

    address = owner_row[f"{address_type}_address"]
    city = owner_row[f"{address_type}_city"]
    state = owner_row[f"{address_type}_state"]
    zip = owner_row[f"{address_type}_zip"]

    if address_specificity == "Remove_address":
        address = None
    elif address_specificity == "Remove_zip":
        address = None
        zip = None
    elif address_specificity == "Remove_city":
        address = None
        zip = None
        city = None

    return get_IDI_owner_contact_for_address(
        first_name,
        middle_name,
        last_name,
        business_name,
        address,
        city,
        state,
        zip,
        authToken
    )

def has_address_type_in_owner_row(owner_row, address_type):
    return owner_row[f"{address_type}_address"] is not None and owner_row[f"{address_type}_address"] != "" and owner_row[f"{address_type}_city"] is not None and owner_row[f"{address_type}_city"] != "" and owner_row[f"{address_type}_state"] is not None and owner_row[f"{address_type}_state"] != "" and owner_row[f"{address_type}_zip"] is not None and owner_row[f"{address_type}_zip"] != ""

def get_IDI_owner_contact_rows(owner_row):

    authToken = get_IDI_authentication_token()
    if authToken is None:
        return None

    print("About to query owner contact for address")
    print(json.dumps(owner_row, indent=4, sort_keys=False, default=str))

    failed_respsones = []

    # Two layers of matching priority: 
    #   - For name, first try to search as a business, then as a person,
    #     then swap the first and last names for person, then omit name entirely and just use address.
    #   - Within name techniques, try "party" then "mailing" then "property" addresses.
    for match_technique in ["Normal_name", "Reverse_name"]:
        for address_type in ["party", "mailing", "property"]:

            if not has_address_type_in_owner_row(owner_row, address_type):
                print(f"Skip address type {address_type} because it is not present in owner row")
                continue

            for address_specificity in ["Full_address", "Remove_street", "Remove_zip", "Remove_city"]:
                response = get_IDI_owner_contact_for_address_type(address_type, owner_row, authToken, match_technique, address_specificity)
                parsedResponse = transform_raw_IDI_json_to_normalized_owner_contact_row(response)
                if parsedResponse is not None:
                    print(f"Found owner contact for match technique {match_technique}, address type {address_type}, and specificity {address_specificity}")
                    return response, failed_respsones, f"{address_type}:{match_technique}:{address_specificity}"
                else:
                    failed_respsones.append(response)

    return None, failed_respsones, None


def transform_raw_IDI_json_to_normalized_owner_contact_row(raw_IDI_json):

    if raw_IDI_json is None:
        return None

    if "result" not in raw_IDI_json:
        return None

    resultList = raw_IDI_json["result"]
    if len(resultList) == 0:
        return None

    if len(resultList) > 1:
        print("Warning: multiple results returned from IDI search")

    phone_numbers = []
    emails = []

    for result in resultList:

        if "business" in result:
            for business in result["business"]:
                if "phones" in business:
                    business_phone_numbers = list(map(lambda phone_number: {
                        "phone_number": phone_number, 
                        "type": "Business", 
                        "is_confirmed": False
                    }, business["phones"]))
                    phone_numbers.extend(business_phone_numbers)

        if "phone" in result:
            person_phone_numbers = list(map(lambda phoneEntry: {
                "phone_number": phoneEntry["number"], 
                "type": phoneEntry.get("type"), 
                "is_confirmed": False
            }, result["phone"]))
            phone_numbers.extend(person_phone_numbers)

        if "email" in result:
            raw_emails = list(map(lambda phoneEntry: phoneEntry["data"], result["email"]))
            raw_emails_to_insert = list(map(lambda num: { "email": num, "is_confirmed": False}, raw_emails))
            emails.extend(raw_emails_to_insert)

    new_row = {}
    if len(phone_numbers) > 0:
        new_row["phone_numbers"] = phone_numbers
    if len(emails) > 0:
        new_row["emails"] = emails

    if "emails" in new_row or "phone_numbers" in new_row:
        return new_row
    else:
        return None
