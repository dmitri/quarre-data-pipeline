#
# Apache beam pipeline that takes zip codes and
# downloads Cherre's tax assessor data for them.
#
#

import apache_beam as beam
import simplejson as json
import requests
import re


from apache_beam.io import fileio
from apache_beam.io import ReadFromBigQuery
from apache_beam.io import WriteToBigQuery
from apache_beam.io.gcp.bigquery_tools import parse_table_schema_from_json
from apache_beam.io.gcp.internal.clients import bigquery
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import GoogleCloudOptions
from apache_beam.options.pipeline_options import StandardOptions
from apache_beam.transforms.trigger import AfterCount, Repeatedly, AccumulationMode
from google.cloud import bigquery
from google.cloud.exceptions import NotFound
from google.oauth2 import service_account



from GetCherreTaxAssessorData import QueryCherreTaxAssessorSource
from LogElements import LogElements
from quarreUtil import *


options = PipelineOptions()
google_cloud_options = options.view_as(GoogleCloudOptions)
google_cloud_options.project = "quarre"
google_cloud_options.job_name = "add-tax-assessor"
google_cloud_options.staging_location = "gs://quarre-staging/dataflow/staging"
google_cloud_options.temp_location = "gs://quarre-staging/dataflow/temp"
options.view_as(StandardOptions).runner = "DirectRunner"  # use this for debugging
#options.view_as(StandardOptions).runner = "DataFlowRunner"


# Need this to run outside of the google cloud environment (e.g. your laptop).
#credentials = service_account.Credentials.from_service_account_file("./quarre-6322d49de7bc.json")


def get_query():

  query = f"""

select zip_interest.zip 
from `quarre.DataResearchFiles.zip_interest` zip_interest
where zip_interest.is_active = true
order by zip_interest.zip

  """
  return query

        
##### Code starts here #####

project_id = "quarre"
client = bigquery.Client(project=project_id)#credentials=credentials)

tax_assessor_dataset_id = "DataResearchFiles"
tax_assessor_table_id = "tax_assessor_experimental_NYS_20220801"
tax_assessor_schema_json_filepath = "./schema/big-query/tax-assessor.json"
tax_assessor_dataset_and_table_id = f"{tax_assessor_dataset_id}.{tax_assessor_table_id}"
tax_assessor_fully_qualified_table_id = f"{project_id}:{tax_assessor_dataset_and_table_id}"

with open(tax_assessor_schema_json_filepath) as json_file:
  target_schema = json.load(json_file)
  create_bq_table_if_not_exists(client, tax_assessor_dataset_id, tax_assessor_table_id, target_schema["fields"])


query = get_query()

source = ReadFromBigQuery(
  query=query, 
  gcs_location="gs://quarre-staging/dataflow/temp",
  use_standard_sql=True)
target = WriteToBigQuery(
  table=tax_assessor_fully_qualified_table_id, 
  schema=target_schema, 
  custom_gcs_temp_location="gs://quarre-staging/dataflow/temp",
  create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
  write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND)

class JsonSink(fileio.TextSink):
  def write(self, record):
    self._fh.write(json.dumps(record, indent=2).encode('utf-8'))


def run():
  with beam.Pipeline(options=options) as p:
    pipeline = (
      p 
      | "ReadZipCodeTable" >> beam.io.Read(source) 
      # | "GroupByZip" >> beam.WindowInto(
      #   beam.window.GlobalWindows(),
      #   trigger=Repeatedly(AfterCount(1)),
      #   accumulation_mode=AccumulationMode.DISCARDING)
      # | "LogElements1" >> beam.ParDo(LogElements())
      | "QueryCherreTaxAssessor" >> beam.ParDo(QueryCherreTaxAssessorSource(tax_assessor_dataset_and_table_id, target_schema["fields"]))
      # | "LogElements2" >> beam.ParDo(LogElements())
      # | "ReadFromGCS" >> beam.io.ReadAllFromText()
      # | "ParseJson" >> beam.Map(json.loads)
      # | "WriteToBigQuery" >> beam.io.Write(target)
      )

if __name__ == "__main__":
  run()
