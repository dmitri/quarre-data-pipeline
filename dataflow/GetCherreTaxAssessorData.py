
from datetime import datetime
import simplejson as json


import apache_beam as beam
from apache_beam.io import filesystems

from google.cloud import bigquery

from quarreUtil import *
from cherreUtil import get_tax_assessor_data_for_zip

def load_gcs_to_big_query(gcs_path, target_table, target_schema):
  print(f"About to load {gcs_path} to bigquery.")
  bq_client = bigquery.Client()

  job_config = bigquery.LoadJobConfig(
    source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON, 
    schema=target_schema, 
    create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
    write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND
  )
  job = bq_client.load_table_from_uri(gcs_path, target_table, job_config=job_config)
  job.result()  # Waits for the job to complete.
  print(f"Loaded {gcs_path} to bigquery.")


#
# Queries Cherre for a given zip code and returns 
# all the tax assessor data for that zip code.
#
#
class QueryCherreTaxAssessorSource(beam.DoFn):
  """
  Query Cherre for tax assessor data.
  """

  def __init__(self, target_table, target_schema):
    print("Initializing QueryCherreTaxAssessorSource")
    self.target_table = target_table
    self.target_schema = target_schema

  def process(self, zip_row):
    zip_code = zip_row["zip"]
    yyyymmdd = datetime.today().strftime('%Y%m%d')
    gcs_path = f"gs://quarre-staging/tax_assessor/zip_{zip_code}/{yyyymmdd}/data.ndjson"
    writer = filesystems.FileSystems.create(gcs_path)
    row_count = get_tax_assessor_data_for_zip(zip_code, writer)
    print(f"From zip {zip_code} wrote {row_count} rows to {gcs_path}")
    writer.close()

    load_gcs_to_big_query(gcs_path, self.target_table, self.target_schema)

    return [gcs_path]
