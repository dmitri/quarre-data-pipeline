
from datetime import datetime
import simplejson as json


import apache_beam as beam

from quarreUtil import *
from piplUtil import transform_raw_Pipl_json_to_normalized_owner_contact_row, get_Pipl_owner_contact_rows
from idiUtil import transform_raw_IDI_json_to_normalized_owner_contact_row, get_IDI_owner_contact_rows


#
# Queries a contact provider for a given name / address as part
# of a Apache Beam dataflow.
#
#
class QueryContactInfoSource(beam.DoFn):
    """
    Query a contact info provider and return the raw API response.
    """

    def __init__(self):
        print("Initializing QueryContactInfoSource")

    def process(self, row):
        api_response, failed_responses, match_technique = get_IDI_owner_contact_rows(row)
        #api_response = get_Pipl_owner_contact_rows(row)
        new_row = { 
            "owner_id": row["owner_id"],
            "quarre_owner_id": row["quarre_owner_id"],
            "tax_assessor_id": row["tax_assessor_id"],
            "api_response": api_response,
            "failed_responses": failed_responses,
            "match_technique": match_technique
        }
        return [new_row]


class NormalizeOwnerContactInfo(beam.DoFn):
    """
    Query an contact info provider to add rows to the owner_contact table for entries in tax_assessor.
    """
    
    def __init__(self):
        print("Initializing AddOwnerContactInfo")
        
    def process(self, row):

        normalized_row = {
            "at_time": datetime.now().strftime("%Y-%m-%d %H:%M:%S.%f"),
            "owner_id": row["owner_id"],
            "quarre_owner_id": row["quarre_owner_id"],
            "tax_assessor_id": row["tax_assessor_id"],
            "contact_source_id": "IDI",
            "match_technique": row["match_technique"]
        }

        #new_row = transform_raw_Pipl_json_to_normalized_owner_contact_row(row)

        if "api_response" in row and row["api_response"] is not None:
            normalized_data = transform_raw_IDI_json_to_normalized_owner_contact_row(row["api_response"])
            if normalized_data is not None:
                normalized_row = { **normalized_row, **normalized_data }

        if "emails" not in normalized_row:
            normalized_row["emails"] = []
        if "phone_numbers" not in normalized_row:
            normalized_row["phone_numbers"] = []

        print("will try to commit")
        print(json.dumps(normalized_row, indent=4, sort_keys=False, default=str))
        
        return [normalized_row]
