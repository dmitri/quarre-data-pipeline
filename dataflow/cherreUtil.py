import requests
import simplejson as json
import time
import socket
from fileUtil import file_to_string
from datetime import datetime
import os

dirname = os.path.dirname(os.path.abspath(__file__))

cherreApiKeyFilename = os.path.join(dirname, "..", "data", "api-key", "cherre-api-key.txt")
apiKey = file_to_string(cherreApiKeyFilename).strip()

def queryGraphQL(queryBody, variables = None):
  endpoint = "https://graphql.cherre.com/graphql"
  timeoutSeconds = 120
  authHeader = f"Bearer {apiKey}"
  headers = { "authorization": authHeader }
  payload = {
    "query": queryBody
  }
  if variables is not None:
    payload["variables"] = variables
  jsonPayload = json.dumps(payload, indent=4, sort_keys=False, default=str)

  retryCount = 15
  waitBetweenRetrySeconds = 30
  while retryCount > 0:
    try:
      r = requests.post(endpoint, data = jsonPayload, headers = headers, timeout = timeoutSeconds)
      if r.status_code == requests.codes.ok:
        return r.json()
      else:
        retryCount -= 1
        print(f"Failed on graphql with status {r.status_code}.")
        print(r.reason)
        print(r.text)
        print(f"Will sleep {waitBetweenRetrySeconds} seconds and retry {retryCount} more times.")
        time.sleep(waitBetweenRetrySeconds)
        waitBetweenRetrySeconds *= 2
    except (socket.timeout, requests.exceptions.ReadTimeout) as e:
      print("Caught socket timeout.")
      print(f"Will sleep {waitBetweenRetrySeconds} seconds and retry {retryCount} more times.")
      retryCount -= 1
      time.sleep(waitBetweenRetrySeconds)
      waitBetweenRetrySeconds *= 2
  raise Exception("GraphQL queries failed even after retries.")


def getOneGraphQLBatch(query, cursorValues, limit, offset, objectType):
  startTime = datetime.now()
  variables = cursorValues.copy()
  variables["limit"] = limit
  variables["offset"] = offset
  print("About to query GraphQL")
  #print("query", query)
  #print("variables", variables)
  rawResult = queryGraphQL(query, variables)
  data = rawResult["data"][objectType]
  totalTime = datetime.now() - startTime
  print(f"Got {len(data)} from graphql in {totalTime.seconds} s")

  #resultJson = json.dumps(data, indent=4, sort_keys=False, default=str)
  #print(resultJson)

  return data


def jsonRowsToString(rows):
  rowsAsString = list(map(lambda x: json.dumps(x) + "\n", rows))
  rowsAsStringEncoded = list(map(lambda x: x.encode(), rowsAsString))
  return rowsAsStringEncoded


def getGraphQLPages(objectType, graphQLQuery, limit, queryVariables, file_writer):

  totalRows = 0
  startTime = datetime.now()

  offset = 0
  firstBatch = getOneGraphQLBatch(graphQLQuery, queryVariables, limit, offset, objectType)

  if len(firstBatch) == 0:
    print("No results in first batch.")
    return totalRows

  json_rows_string = jsonRowsToString(firstBatch)
  file_writer.writelines(json_rows_string)

  lastBatchSize = len(firstBatch)
  totalRows += lastBatchSize

  while lastBatchSize == limit:
    offset += limit

    nextBatch = getOneGraphQLBatch(graphQLQuery, queryVariables, limit, offset, objectType)
    lastBatchSize = len(nextBatch)
    if lastBatchSize == 0:
      break

    json_rows_string = jsonRowsToString(nextBatch)
    file_writer.writelines(json_rows_string)

    totalTime = datetime.now() - startTime
    totalRows += lastBatchSize
    totalSeconds = totalTime.seconds
    if totalSeconds != 0:
      rowsPerSecond = totalRows / totalSeconds
      print(f"Retrieved {totalRows}. Avg {rowsPerSecond} rows/second.")
    else:
      print("Weird seconds turned to zero!!")
      print(totalRows, totalTime)
  
  return totalRows


def get_tax_assessor_data_for_zip(zipCode, file_writer):
  objectType = "tax_assessor"
  queryFile = os.path.join(dirname, "..", "queries", "cherre-graph-ql", "tax_assessor_by_zip.graphql")
  query = file_to_string(queryFile)
  zipVar = {
    "zip": zipCode
  }
  return getGraphQLPages(objectType, query, 500, zipVar, file_writer)

def get_nyscef_court_data_for_date_range(beginDate, endDate, file_writer):
  objectType = "ny_court_nyscef"
  queryFile = os.path.join(dirname, "..", "queries", "cherre-graph-ql", "nyscef_court.graphql_by_date.graphql")
  query = file_to_string(queryFile)
  vars = {
    "beginDate": beginDate
  }
  return getGraphQLPages(objectType, query, 500, vars, file_writer)

def get_ny_court_cases_data_for_date_range(beginDate, endDate, file_writer):
  objectType = "ny_court_cases"
  queryFile = os.path.join(dirname, "..", "queries", "cherre-graph-ql", "ny_court_cases_by_date.graphql")
  query = file_to_string(queryFile)
  vars = {
    "beginDate": beginDate
  }
  return getGraphQLPages(objectType, query, 500, vars, file_writer)

def get_ny_court_jdg_data_for_date_range(beginDate, endDate, file_writer):
  objectType = "ny_court_jdg"
  queryFile = os.path.join(dirname, "..", "queries", "cherre-graph-ql", "ny_court_jdg_by_date.graphql")
  query = file_to_string(queryFile)
  vars = {
    "beginDate": beginDate
  }
  return getGraphQLPages(objectType, query, 500, vars, file_writer)

def get_ny_court_cas_data_for_date_range(beginDate, endDate, file_writer):
  objectType = "ny_court_cas"
  queryFile = os.path.join(dirname, "..", "queries", "cherre-graph-ql", "ny_court_cas_by_date.graphql")
  query = file_to_string(queryFile)
  vars = {
    "beginDate": beginDate
  }
  return getGraphQLPages(objectType, query, 500, vars, file_writer)


def get_residential_listings_data_for_zip(zipCode, file_writer):
  objectType = "residential_listings_public_master"
  queryFile = os.path.join(dirname, "..", "queries", "cherre-graph-ql", "residential_listings_by_zip.graphql")
  query = file_to_string(queryFile)
  zipVar = {
    "zip": zipCode
  }
  return getGraphQLPages(objectType, query, 500, zipVar, file_writer)
