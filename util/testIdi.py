import simplejson as json
import base64
import re
from webbrowser import get
import requests


def get_IDI_authentication_token():

    authenticationUrl = "https://login-api-test.idicore.com/apiclient"
    clientid = "api-client@quarre_test" 
    clientsecret = "nEB3KMDFdSse64aaCE!%p5kM9cH6LP6t%J64i8dWkQikG2Zk$dC7NSRdgwXWnMy7"
    #clientsecret = "NQQcdSe7X2vzQrCzpath3XTCDNEiyqmWQUmKyrqAj%PH6MzWJBr!QKrrxRreHRGv"

    # Get the authentication token.
    authString = base64.b64encode((f"{clientid}:{clientsecret}").encode('ascii'))

    payload = {
        "glba": "otheruse",
        "dppa": "none"
    }

    jsonPayload = json.dumps(payload, indent=4, sort_keys=False, default=str)

    authHeaderValue = f"Basic {authString.decode('ascii')}"

    headers = {
        "Authorization": authHeaderValue,
        "Content-type": "application/json"
    }

    #print(json.dumps(headers, indent=4, sort_keys=False, default=str))

    response = requests.request("POST", authenticationUrl, data=jsonPayload, headers=headers)
    if (response.status_code != 200):
        print(f"Authentication failed with error {response.status_code}")
        return None

    authToken = response.text

    return authToken


def get_IDI_owner_contact_for_address(firstName, middleName, lastName, businessName, address, city, state, zip, authToken):

    searchUrl = "https://api-test.idicore.com/search"

    payload = {
        "fields": ["name", "phone", "address", "email"],
        "address": address,
        "city": city,
        "state": state,
        "zip": zip,
        "lastName": lastName,
        "firstName": firstName,
        "middleName": middleName,
        "businessName": businessName
    }

    for k, v in dict(payload).items():
        if v is None:
            del payload[k]

    if "firstName" in payload:
        payload["nicknamesearch"] = True

    headers = {
        "authorization": authToken,
        "content-type": "application/json",
        "accept": "application/json"
    }

    jsonPayload = json.dumps(payload, indent=4, sort_keys=False, default=str)

    #print("will query IDI for owner contact")
    #print(json.dumps(payload, indent=4, sort_keys=False, default=str))

    response = requests.request("POST", searchUrl, data=jsonPayload, headers=headers)


    if (response.status_code != 200):
        print(f"Search failed with error {response.status_code}")
        print("Request payload:")
        print(json.dumps(payload, indent=4, sort_keys=False, default=str))
        print("Response:")
        print(response.text)
        return None

    # print(json.dumps(response.json(), indent=4, sort_keys=False, default=str))
    # print(response.status_code)

    return response.json()




authToken = get_IDI_authentication_token()
if authToken is not None:
    print("Authentication token:")
    print(authToken)

    address = "11321 JIM CT"
    city = "RIVERVIEW"
    state = "FL"
    zip = "33569"
    businessName = "FA B MARKETING"

    response = get_IDI_owner_contact_for_address(None, None, None, businessName, "123 Main St", "Anytown", "CA", "90210", authToken)
    if response is not None:
        print("IDI owner contact:")
        print(json.dumps(response, indent=4, sort_keys=False, default=str))



  