* Initial setup
To setup the repo, init the venv and download the dependencies.
```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

* Notes 
To run dataflow jobs, we now have to set this environment variable.
```bash
export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
```

* IDI
To run IDI we use Apache Beam. This code also needs to be run from our 'idi' cloud host that has its IP allowed by IDI.

Note that this code is a work in progress and we only process batches of 500 now per invocation.
We need to learn how to actually stream results (in Beam I think this is called 'windowing').
```bash
python dataflow/add-contact-info-for-owners.py
```
To update the input query we edit that file, specically changing this block:
```sql
with owner_interest_union as (

select distinct f.*
from `quarre.DataResearchFiles.Owner_Interest_Final` f
left join `quarre.DataResearchFiles.zip_interest` z on z.zip = f.property_zip and z.is_active is true
left join `quarre.DataResearchFiles.Combined_AVM` a on a.tax_assessor_id = f.tax_assessor_id and a.max_date = 1
where query_type like 'NY%'
#and (query_type like 'MIAMI%' or query_type like 'BROWARD%' or query_type like 'PALM%' )
#OR (query_type like 'HILLSBOROUGH%' or query_type like 'PINELLAS%' or query_type like 'ORANGE%' or query_type like 'DUVAL%')
#)
#and run_time > '2022-12-01'
and (a.avm_value = 0 or a.avm_value > 500000 or a.AVM_value is null)
and f.description not like 'Condo%' 
and f.query_type not like '%FOREIGN%' 
and (property_zip in ('11561','11569','11567','11201','11231','11215','11217','11232','11233','11220','11232','11235','11238','11710','11516','11783','11793','11101','11105','11102','11103','11104','11106','11109','11120','11357','11375') 
or property_zip in (select distinct zip from `quarre.DataResearchFiles.zip_interest` where lower(county) in ('nassau county','new york county') and is_active is true))
#and owner_id not like '%::I::%'
and property_city not in ('ROSEDALE','ELMONT','FAR ROCKAWAY','BRONX','GLEN OAKS','NEW HYDE PARK','ROOSEVELT')
AND QUERY_TYPE not like '%REBNY%'
and f.description not like 'Cooperative%'
AND MAILING_STREET != PROPERTY_STREET

)
```

* Cherre tax assessor
Cherre tax assessor data also uses Apache Beam. The input table is `zip_interest` in Big Query. We
filter the query by zip code, by setting `is_active`, e.g. to just query for Brooklyn:
```sql
update `quarre.DataResearchFiles.zip_interest`
set is_active = false;

update `quarre.DataResearchFiles.zip_interest`
set is_active = true
where county = 'Kings County';
```

* Cherre courts
Cherre court data is handled by the script.
```bash
python dataflow/copy-cherre-to-bigquery.py 
```
This file needs to be manually edited to choose the court dataset that is being downloaded.

We should add a command line option to pick the dataset. Also we need to edit it to support a begin date from the
command line.

* Pacer
Pacer download is handled by the script.
```bash
python dataflow/copy-pacer-to-bigquery.py
```
Note that Pacer only allows 100 pages of results. We should enhance this to download for a date range (now
it just has begin date). Then make the date range something small enough that we won't hit 100 pages of results
(e.g. download 1 date at a time in a for loop).

* 